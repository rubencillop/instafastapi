from fastapi import FastAPI, Response, status, HTTPException, Depends
from typing import List
import crud
import os
import instaloader

#uvicorn main:app --reload

app = FastAPI()
L = instaloader.Instaloader()

@app.on_event("startup")
def startup_event():

    try:
        crud.instagram_login(L)
    except Exception as e: 
        message = str(e)
        print (message)

@app.get("/status")
async def root():
    return Response(status_code = status.HTTP_200_OK)

@app.get("/insta/{username}", status_code=200)
async def read_followers(username, response: Response):

    try:
        return_response = crud.instagram_followers(L, username)
        response.status_code = status.HTTP_200_OK
        return {"followers": return_response}
    except Exception as e: 
        message = str(e)
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": message}


@app.get("/post/{username}", status_code=200)
async def read_post(username, response: Response):

    try:
        return_response = crud.instagram_post(L, username)
        response.status_code = status.HTTP_200_OK
        return {"followers": return_response}
    except Exception as e: 
        message = str(e)
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": message}