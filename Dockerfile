FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

RUN apt update; apt install -y vim

COPY ./app /app
COPY ./requirements.txt /
RUN pip install -r /requirements.txt 