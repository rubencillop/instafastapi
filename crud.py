import os
import instaloader

def instagram_login(L):

    LOGIN_USER = os.getenv("LOGIN_USER")
    LOGIN_PASSWORD = os.getenv("LOGIN_PASSWORD")

    L.login(LOGIN_USER, LOGIN_PASSWORD)
    #print ("Login OK")
    return L

def instagram_followers(L , PROFILE):

    profile = instaloader.Profile.from_username(L.context, PROFILE)
    print ("Profile " + str(PROFILE) + " Read OK")
    return profile.followers

def instagram_post(L, PROFILE):
    postear="No se pueden obtener post"
    profile = instaloader.Profile.from_username(L.context, PROFILE)
    for post in profile.get_posts(): 
        #guardar un unico post en str
        postear = str(post)
        break
    print ("Profile Post " + str(PROFILE) + " Read OK")
    return postear